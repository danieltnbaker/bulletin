package dev.dandroid.bulletin

import dev.dandroid.bulletin.data.mapper.StatusMapper
import dev.dandroid.bulletin.data.model.Status
import dev.dandroid.bulletin.data.model.User
import org.junit.Test

class StatusMapperTest {

    @Test
    fun `get status object from api and convert to domain tweet object`() {
        val mapper = StatusMapper()

        val tweets = mapper.mapToDomain(listOf(status))

        assert(tweets.first().handle == "UserHandle")
        assert(tweets.first().tweet == "This is the tweet")
        assert(tweets.first().date == "Friday 7 February 2020 - 4:09 PM")
    }

    companion object {
        private val user = User("UserId", "UserHandle")
        private val status = Status("Fri Feb 07 16:09:17 +0000 2020", "tweetId", "This is the tweet", user, 1, 1)
    }
}