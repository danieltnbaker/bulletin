package dev.dandroid.bulletin.ui.main

import android.os.Bundle
import dev.dandroid.bulletin.R
import dev.dandroid.bulletin.base.BaseActivity
import dev.dandroid.bulletin.ui.list.TweetListFragment

class MainActivity : BaseActivity() {

    override fun layoutRes(): Int {
        return R.layout.activity_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.screenContainer, TweetListFragment()).commit()
        }
    }
}