package dev.dandroid.bulletin.ui.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.status_list_item.view.*
import dev.dandroid.bulletin.R
import dev.dandroid.bulletin.data.model.Tweet
import dev.dandroid.bulletin.ui.list.TweetListAdapter.TweetViewHolder

class TweetListAdapter(val data: List<Tweet>) : RecyclerView.Adapter<TweetViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TweetViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.status_list_item, parent, false)
        return TweetViewHolder(view)
    }

    override fun onBindViewHolder(holder: TweetViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    inner class TweetViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(tweet: Tweet) {
            itemView.statusUser.text = tweet.handle
            itemView.statusDate.text = tweet.date
            itemView.statusText.text = tweet.tweet
        }
    }
}