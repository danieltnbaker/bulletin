package dev.dandroid.bulletin.ui.list

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.tweet_list_fragment.*
import dev.dandroid.bulletin.R
import dev.dandroid.bulletin.base.BaseFragment
import dev.dandroid.bulletin.util.ViewModelFactory
import javax.inject.Inject

class TweetListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private var tweetListViewModel: TweetListViewModel? = null

    override fun layoutRes(): Int = R.layout.tweet_list_fragment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tweetListViewModel = ViewModelProviders.of(this, viewModelFactory).get(TweetListViewModel::class.java)
        tweetListViewModel?.let { viewModel ->

            viewModel.loadTweets.observe(this, Observer {
                it?.let { tweets ->
                    recyclerView.adapter = TweetListAdapter(tweets)
                    recyclerView.layoutManager = LinearLayoutManager(context)
                    recyclerView.visibility = View.VISIBLE
                }
            })
            viewModel.loadError.observe(this, Observer {
                if (it == true) {
                    recyclerView.visibility = View.GONE
                    errorTextView.visibility = View.VISIBLE
                    errorTextView.text = getString(R.string.status_list_error_message)
                } else {
                    errorTextView.visibility = View.GONE
                    errorTextView.text = null
                }
            })
            viewModel.loading.observe(this, Observer {
                if (it == true) {
                    swipeRefreshLayout.isRefreshing = true
                    recyclerView.visibility = View.GONE
                    errorTextView.visibility = View.GONE
                } else {
                    swipeRefreshLayout.isRefreshing = false
                }
            })

            swipeRefreshLayout.setOnRefreshListener { viewModel.loadTweets() }

            viewModel.loadTweets()
        }
    }
}