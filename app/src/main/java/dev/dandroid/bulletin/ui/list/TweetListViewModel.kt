package dev.dandroid.bulletin.ui.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import dev.dandroid.bulletin.data.model.Tweet
import dev.dandroid.bulletin.data.domain.GetTweetsUseCase
import javax.inject.Inject

class TweetListViewModel @Inject constructor(private val getTweetsUseCase: GetTweetsUseCase) : ViewModel() {

    private var disposable = CompositeDisposable()

    val loadTweets: MutableLiveData<List<Tweet>> = MutableLiveData()
    val loadError: MutableLiveData<Boolean> = MutableLiveData()
    val loading: MutableLiveData<Boolean> = MutableLiveData()

    fun loadTweets() {
        loading.value = true

        disposable.add(getTweetsUseCase.getTweets()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Tweet>>() {
                    override fun onSuccess(value: List<Tweet>) {
                        loadError.value = false
                        loadTweets.value = value
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        loadError.value = true
                        loading.value = false
                    }
                }))
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}