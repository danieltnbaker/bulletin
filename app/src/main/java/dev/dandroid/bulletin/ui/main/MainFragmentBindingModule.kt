package dev.dandroid.bulletin.ui.main

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dev.dandroid.bulletin.ui.list.TweetListFragment

@Module
abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    abstract fun provideListFragment(): TweetListFragment
}