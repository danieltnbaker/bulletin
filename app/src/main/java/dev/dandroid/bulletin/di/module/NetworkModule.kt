package dev.dandroid.bulletin.di.module

import dagger.Module
import dagger.Provides
import dev.dandroid.bulletin.data.rest.HeaderInterceptor
import dev.dandroid.bulletin.data.rest.StatusService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(headerInterceptor: HeaderInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(headerInterceptor)
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder().baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofitService(retrofit: Retrofit): StatusService {
        return retrofit.create(StatusService::class.java)
    }

    companion object {
        private const val BASE_URL: String = "https://api.twitter.com/1.1/"
    }
}