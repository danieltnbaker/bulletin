package dev.dandroid.bulletin.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dev.dandroid.bulletin.di.util.ViewModelKey
import dev.dandroid.bulletin.ui.list.TweetListViewModel
import dev.dandroid.bulletin.util.ViewModelFactory

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(TweetListViewModel::class)
    abstract fun bindListViewModel(tweetListViewModel: TweetListViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}