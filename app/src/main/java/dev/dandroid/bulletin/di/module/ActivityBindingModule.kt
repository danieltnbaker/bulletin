package dev.dandroid.bulletin.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dev.dandroid.bulletin.ui.main.MainActivity
import dev.dandroid.bulletin.ui.main.MainFragmentBindingModule

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [MainFragmentBindingModule::class])
    abstract fun bindMainActivity(): MainActivity
}