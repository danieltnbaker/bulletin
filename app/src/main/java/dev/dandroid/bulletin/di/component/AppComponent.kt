package dev.dandroid.bulletin.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dev.dandroid.bulletin.base.BaseApplication
import dev.dandroid.bulletin.di.module.ApplicationModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class
])
interface AppComponent : AndroidInjector<BaseApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: BaseApplication): Builder

        fun build(): AppComponent
    }
}