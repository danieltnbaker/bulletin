package dev.dandroid.bulletin.data.domain

import dev.dandroid.bulletin.data.mapper.StatusMapper
import dev.dandroid.bulletin.data.model.Tweet
import dev.dandroid.bulletin.data.rest.StatusRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetTweetsUseCase @Inject constructor(private val statusRepository: StatusRepository,
                                           private val statusMapper: StatusMapper) {

    fun getTweets(): Single<List<Tweet>> {
        return statusRepository.getStatuses()
                .observeOn(Schedulers.computation())
                .map { statusMapper.mapToDomain(it) }
    }
}