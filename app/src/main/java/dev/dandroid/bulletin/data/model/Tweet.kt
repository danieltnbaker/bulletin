package dev.dandroid.bulletin.data.model

data class Tweet(
        val handle: String,
        val tweet: String,
        val date: String
)