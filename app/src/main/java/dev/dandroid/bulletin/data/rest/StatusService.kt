package dev.dandroid.bulletin.data.rest

import io.reactivex.Single
import dev.dandroid.bulletin.data.model.Status
import retrofit2.http.GET
import retrofit2.http.Query

interface StatusService {

    @GET("lists/statuses.json")
    fun getStatuses(
            @Query("list_id") listId: String = "871746761387323394",
            @Query("tweet_mode") tweetMode: String = "extended",
            @Query("include_entities") includeEntities: Int = 1,
            @Query("count") count: Int = 10
    ): Single<List<Status>>
}