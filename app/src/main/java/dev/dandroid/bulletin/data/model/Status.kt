package dev.dandroid.bulletin.data.model

import com.google.gson.annotations.SerializedName

data class Status (
    @SerializedName("created_at")
    val created: String,
    @SerializedName("id_str")
    val tweetId: String,
    @SerializedName("full_text")
    val fullText: String,
    @SerializedName("user")
    val user: User,
    @SerializedName("retweet_count")
    val retweetCount: Int,
    @SerializedName("favorite_count")
    val favoriteCount: Int
)