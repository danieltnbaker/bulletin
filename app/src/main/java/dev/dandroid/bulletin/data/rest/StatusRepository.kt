package dev.dandroid.bulletin.data.rest

import io.reactivex.Single
import dev.dandroid.bulletin.data.model.Status
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class StatusRepository @Inject constructor(private val statusService: StatusService) {

    fun getStatuses(): Single<List<Status>> {
        return statusService.getStatuses()
                .subscribeOn(Schedulers.io())
    }
}