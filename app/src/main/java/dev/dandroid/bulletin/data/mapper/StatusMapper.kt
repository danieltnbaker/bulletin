package dev.dandroid.bulletin.data.mapper

import dev.dandroid.bulletin.data.model.Status
import dev.dandroid.bulletin.data.model.Tweet
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class StatusMapper @Inject constructor() {

    fun mapToDomain(statusList: List<Status>): List<Tweet> {
        val tweetList = mutableListOf<Tweet>()
        statusList.forEach {
            tweetList.add(Tweet(it.user.screenName, it.fullText, getFormattedDate(it.created)))
        }
        return tweetList
    }

    private fun getFormattedDate(dateString: String): String {
        val locale = Locale.getDefault()
        val simpleDateFormatOld = SimpleDateFormat(OLD_DATE_FORMAT, locale)
        val simpleDateFormatNew = SimpleDateFormat(NEW_DATE_FORMAT, locale)
        val date = simpleDateFormatOld.parse(dateString) ?: Date()
        return simpleDateFormatNew.format(date)
    }

    companion object {
        private const val OLD_DATE_FORMAT: String = "EEE MMM dd HH:mm:ss z yyyy"
        private const val NEW_DATE_FORMAT: String = "EEEE d MMMM yyyy - h:mm a"
    }
}