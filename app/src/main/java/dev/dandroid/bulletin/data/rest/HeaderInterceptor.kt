package dev.dandroid.bulletin.data.rest

import okhttp3.Interceptor
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HeaderInterceptor @Inject constructor() : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val sonicRequestRequest = chain.request().newBuilder()
                .header(HEADER_AUTHORIZATION, HEADER_BEARER)
                .build()
        return chain.proceed(sonicRequestRequest)
    }

    companion object {

        private const val HEADER_AUTHORIZATION = "Authorization"
        private const val HEADER_BEARER = "Bearer AAAAAAAAAAAAAAAAAAAAAF7w0wAAAAAAb6kdTQSU%2F5EmGAMD917iN7rZuVE%3D9ssQYqmHSgDTUgLDOW3k155qYVOxrGUaDSeOrW3IqvHeSoRYiu"
    }
}