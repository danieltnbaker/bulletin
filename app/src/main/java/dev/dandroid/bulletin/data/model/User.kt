package dev.dandroid.bulletin.data.model

import com.google.gson.annotations.SerializedName

data class User(
        @SerializedName("id_str")
        val userId: String,
        @SerializedName("screen_name")
        val screenName: String
)