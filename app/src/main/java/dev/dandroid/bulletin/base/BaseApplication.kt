package dev.dandroid.bulletin.base

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import dev.dandroid.bulletin.di.component.DaggerAppComponent

class BaseApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<BaseApplication> {
        val component = DaggerAppComponent.builder()
                .application(this)
                .build()
        component.inject(this)
        return component
    }
}